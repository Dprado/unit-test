﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Tests.Controllers
{

    [TestClass]
    public class CalculatorTest
    {
        
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }

        private Calculator _SystemUnderTest;

        public Calculator SystemUnderTest
        {
            get
            {
                if(_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }
                return _SystemUnderTest;
            }


        }

 
        [TestMethod]
        public void Add()
        {

            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 5;

            //Act (Actuar)


            int actual = SystemUnderTest.Add(value1,value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Divide()
        {
            //Arranque (Organizar)
            int value3 = 8;
            int value4 = 4;
            int expected = 2;

            //Actuar (Actuar)
            int actual = SystemUnderTest.Divide(value3, value4);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        
        [TestMethod]
        public void Multiply()
        {
            //Arranque (Organizar)
            int value5 = 5;
            int value6 = 5;
            int expected = 25;

            //Actuar (Actuar)
            int actual = SystemUnderTest.Multiply(value5, value6);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Subtract()
        {
            //Arrange (Organizar)
            int value7 = 5;
            int value8 = 2;
            int expected = 3;

            //Act (Actuar)

            int actual = SystemUnderTest.Subtract(value7, value8);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");
        }



    }
}

